// Dependencies
var express = require('express');
require('request').debug = true;
var http = require('http');
var path = require('path');
var socketIO = require('socket.io');
var app = express();
var server = http.Server(app);
var io = socketIO(server);

app.set('port', 80);

app.use('/static', express.static(path.join(__dirname, 'static')))
app.use('/scripts', express.static(path.join(__dirname, 'scripts'), {
    etag: false
}))

app.use(function(req, res, next) {
    console.log('recieved request');
    next();
});



//app.use('/static', express.static('/static'));
// Routing
app.get('/', function(request, response) {
    console.log('GET from: ', request.connection.remoteAddress); 
  response.sendFile(path.join(__dirname, 'index.html'));
});
// Starts the server.
server.listen(80, '0.0.0.0', function() {
  console.log('Starting server on port 80');
});

var masterId = 0;
var slaveSocket = null;
io.on('connection', function(socket) {
    if (masterId == 0) {
        console.log('master connected');
        socket.emit('log', 'you are master');
        socket.emit()

        masterId = socket.id;
        socket.on('update', function(data) {
            if (slaveSocket != null) {
                slaveSocket.emit('update', data);
            }
        });
        socket.on('objects', function(data) {
            if (slaveSocket != null) {
                slaveSocket.emit('objects', data);
            }
        })
        socket.on('disconnect', function() {
            console.log('master disconnected');
            masterId = 0;
        })
    } else {
        slaveSocket = socket;
        socket.emit('log', 'you are slave');
        socket.emit('message', 'SLAVE');
        console.log('slave connected');
    }
});

// setInterval(function() {
//     io.sockets.emit('message', 'hi!');
// }, 1000);
