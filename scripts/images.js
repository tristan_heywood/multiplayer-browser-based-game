var ballImage = new Image();
ballImage.src = '/static/assets/ball3.png';

var redBalls = new Image();
redBalls.src = '/static/assets/redBalls.png';

var greenBalls = new Image();
greenBalls.src = '/static/assets/greenBalls.png';

var trayImage = new Image();
trayImage.src = '/static/assets/breakout.png';

var image1 = new Image();
image1.onload = function() {
    console.log('image1 loaded');
    spritesLoaded = true;
};
image1.src = '/static/assets/sprites1lighter.png';

var shipImage = new Image();
shipImage.src = '/static/assets/spaceships3.png';

var backgroundImage = new Image();
backgroundImage.src = '/static/assets/background2darker.jpg';
