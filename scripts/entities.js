var constants = {
    BALL_BASE_VELOCITY: 0.0003,
    playerHealthBarHeight: 0.01,

    shipCols: 5,
    shipRows: 3,
};
constants.SLED_WIDTH = 0.15;
constants.SLED_WIDTHs = canvas.width;

function extend(o, newProps, doInit = false) {
    var o =  Object.assign(Object.create(o), newProps);
    if (doInit)
        o.init(o);
    return o;
}


var gameObject = {
    name: "gameObject",
    x: 0,
    y: 0,
    vy: 0,
    vx: 0,
    sprite: {
        image: null,
        x: 0, y: 0, w: 0, h: 0,
        dw: 0, dh: 0, //draw width and draw height
    },
    destroyed: false,
    shape: "particle",
    draw: function() {
        return
    },
    update: function(dt, that) {
        that.x += that.vx * dt;
        that.y += that.vy * dt;
    },
    onCollision: {},
    explode: function() {
        this.destroyed = true;
        objects.push(make_explosion(this));
        soundManager.play('poof');
    },
    init: function() {
        return this;
    },
    draw_location: function() {
        ctx.beginPath();
        ctx.lineWidth = '5';
        ctx.strokeStyle = 'white';
        ctx.arc(this.x*cw, this.y*ch, 4, 0, 2*Math.PI);
        ctx.stroke();
    },
    assign: function(other) {
        return Object.assign(this, other);
    }
}
var particleObject = extend(gameObject, {
    name: "particleObject",
    shape: "particle",
    draw: function() {
        s = this.sprite;
        ctx.drawImage(s.image, s.x, s.y, s.w, s.h, this.x * cw, this.y * ch, s.dw, s.dh);
    },
    draw_outline: function() {
        ctx.beginPath();
        ctx.lineWidth = '5';
        ctx.strokeStyle = 'white';
        ctx.arc(this.x*cw, this.y*ch, 4, 0, 2*Math.PI);
        ctx.stroke();

    },
    collisionDetector: {
        particle: function(p1, p2) {
            return false;
        },
        rectangle: function(p, r) {
            return p.x > r.x && p.x < r.x + r.w && p.y > r.y && p.y < r.y + r.h;
        },
        circle: function(p, c) {
            dist = function(o1, o2) {
                return Math.pow(Math.pow(o1.x - o2.x, 2) + Math.pow(o1.y - o2.y, 2), 0.5);
            }
            return dist(p, c) < c.r;
        }
    }
});
var rectangleObject = extend(gameObject, {
    name: "rectangleObject",
    w: 0,
    h: 0,
    shape: "rectangle",
    draw: function() {
        s = this.sprite;
        ctx.drawImage(s.image, s.x, s.y, s.w, s.h, this.x * cw, this.y * ch, this.w * cw, this.h * ch);
    },
    draw_outline: function() {
        ctx.beginPath();
        ctx.lineWidth = '5';
        ctx.strokeStyle = 'red';
        ctx.rect(this.x*cw, this.y*ch, this.w*cw, this.h*ch);
        ctx.stroke();
        this.draw_location();

    },
    collisionDetector: {
        particle: function(r, p) {
            return particleObject.collisionDetector.rectangle(p, r);
        },
        rectangle: function(r1, r2) {
            return false;
        },
        circle: function(r, c) {
            return c.x + c.r > r.x
                && c.x - c.r < r.x + r.w
                && c.y + c.r > r.y
                && c.y - c.r < r.y + r.h
        }
    }
});
var circleObject = extend(gameObject, {
    name: "circleObject",
    r: 0,
    shape: "circle",
    useDrawSize: false,
    draw: function() {
        s = this.sprite;
        if (this.useDrawSize) {
            ctx.drawImage(s.image, s.x, s.y, s.w, s.h, this.x*cw - this.sprite.dw/2, this.y*ch - this.sprite.dh/2, this.sprite.dw, this.sprite.dh);
        } else {
            ctx.drawImage(s.image, s.x, s.y, s.w, s.h, (this.x - this.r)*cw, (this.y - this.r)*ch, this.r*2*cw, this.r*2*cw);
        }

    },
    draw_outline: function() {
        ctx.beginPath();
        ctx.arc(this.x*cw, this.y*ch, this.r*cw, 0, 2*Math.PI);
        ctx.lineWidth = 5;
        ctx.strokeStyle = 'red';
        ctx.stroke();
        this.draw_location();

    },
    collisionDetector: {
        particle: function(c, p) {
            return particleObject.collisionDetector.circle(p, c);
        },
        rectangle: function(c, r) {
            return rectangleObject.collisionDetector.circle(r, c);
        },
        circle: function(c1, c2) {
            return false;
        },
    }
});

var bulletObject = extend(particleObject, {
    name: "bullet",
    sprite: {
        image: image1,
        x: 0, y: 50, w: 9, h:5,
        dw: 9, dh: 5
    },
    onCollision: {
        ball: function(bullet, ball) {
            ball.vy = -Math.abs(ball.vy) - 0.0001;
            bullet.explode();
        }
    }
});

var bombObject = extend(circleObject, {
    name: "bomb",
    r: 0.02,
    useDrawSize: true,
    sprite: {
        image: image1,
        x: 0, y: 39, w: 18, h: 8,
        dw: 18*1.4, dh: 8*1.4
    },
    onCollision: {
        bullet: function(bomb, bullet) {
            bomb.explode();
            bullet.destroyed = true;
        }
    }
});
function make_bomb(atObj, side) {
    return extend(bombObject, {
        name: "bomb",
        x: atObj.x + atObj.w/2,
        y: atObj.y + atObj.h/2,
        vy: side == "bottom" ? -0.0001 : 0.0001,
        side: side,
    });
}

var hbh = constants.playerHealthBarHeight;
var playerObject = extend(rectangleObject, {
    x: 0,
    w: 1,
    h: 1,
    draw: function() {},
    onCollision: {
        bomb: function(player, bomb) {
            player.healthBar.value -= 0.1;
            bomb.explode();
        },
        bullet: function(player, bullet) {
            player.healthBar.value -= 0.005;
            bullet.explode();
        },
        ball: function(player, ball) {
            player.healthBar.value -= Math.abs(ball.vy) * 100;
            objects.push(make_explosion(ball));
            soundManager.play('poof');
            ball.bounce(player.name == 'bottomPlayer' ? -Math.abs(ball.vy) : Math.abs(ball.vy));


        }
    },
});

var bottomPlayer = extend(playerObject, {
    name: "bottomPlayer",
    y: 1 - hbh,
    healthBar: make_healthBar(0, (1 - hbh), 1, hbh),
    side: "bottom",

});
var topPlayer = extend(playerObject, {
    name: "topPlayer",
    y: -playerObject.h + hbh,
    healthBar: make_healthBar(0, 0, 1, hbh),
    side: 'top',

});

var sledObject = extend(rectangleObject, {
    name: "sledObject",
    x: 0.5 - 0.15/2,
    w: 0.15,
    h: 0.15 * 33/136,
    isShooting: false,
    timeSinceBullet: 0,
    update: function(dt, that) {
        that.timeSinceBullet += dt;
        if (that.isShooting && that.timeSinceBullet > 100) {
            that.shoot(true);
        }
        that.x = Math.max(0, that.x);
        that.x = Math.min(1 - that.w/2, that.x);
        sledObject.__proto__.update(dt, that);
    },
    sprite: {
        image: trayImage,
        x: 0, y: 280, w: 136, h:33
    },
    onCollision: {
        ball: function(sled, ball) {
            ball.bounce(sled.side == 'bottom' ? -Math.abs(ball.vy): Math.abs(ball.vy));
            ball.side = sled.side;
        }
    },
    init: function(that) {
        make_key_listener = function(setShooting, setSpeed, that) {
            return function(e) {
                switch(e.keyCode) {
                    case that.controlKeyCodes.shoot:
                        that.isShooting = setShooting;
                        if (setShooting)
                            that.shoot();
                        break;
                    case that.controlKeyCodes.left:
                        that.vx = -setSpeed;
                        break;
                    case that.controlKeyCodes.right:
                        that.vx = setSpeed;
                        break;
                }
            }
        };
        document.addEventListener("keydown", make_key_listener(true, 0.001, that));
        document.addEventListener("keyup", make_key_listener(false, 0, that));
        // return this.__proto__.init();
    },
    shoot: function(lockout = true) {
        if (lockout)
            this.timeSinceBullet = 0;
        soundManager.play('pew');
        objects.push(extend(bulletObject, {
            name: "bullet",
            x: this.x + this.w/2,
            y: this.side == "bottom" ? this.y : this.y + this.h,
            vy: this.side == "bottom" ? -4/16/1000 : 4/16/1000,
            side: this.side,
        }));
    }


});
var bottomSled = extend(sledObject, {
    name: "bottomSled",
    y: 1 - hbh - sledObject.h,
    side: "bottom",
    controlKeyCodes: {
        shoot: 38, left: 37, right: 39, //arrow keys
    },
    init: function() {
        document.addEventListener("mousemove", (function(that) {
            return function(e) {
                var relX = (e.clientX - canvas.offsetLeft)/cw;
                if (relX > 0 && relX < 1 - that.w/2) {
                    that.x = relX - that.w/2;
                }
            };
        })(this));
        document.addEventListener("mousedown", this.make_click_listener(true));
        document.addEventListener("mouseup", this.make_click_listener(false));
        return this.__proto__.init(this);
    },
    make_click_listener: function (setShooting) {
        return (function(that) {
            return function(e) {
                if (setShooting) that.shoot(false);
                that.isShooting = setShooting;
            };
        })(this);
    },
}, true);
var topSled = extend(sledObject, {
    name: "topSled",
    y: hbh,
    side: 'top',
    controlKeyCodes: {
        shoot: 87, left: 65, right: 68, //w a d
    },
}, true);

var shipObject = extend(rectangleObject, {
    name: "ship",
    w: 0.12,
    h: 0.12 * 53/153,
    onCollision: {
        ball: function(ship, ball) {
            if (ball.side != "neutral") {
                ball.bounce(-ball.vy);
                ship.explode();
            }
        },
        bullet: function(ship, bullet) {
            ship.healthBar.value -= 0.05;
            bullet.explode();
            if (ship.healthBar.value <= 0) {
                ship.explode();
            }
        }
    },
    explode: function() {
        this.destroyed = true;
        objects.push(make_bomb(this, this.side == "bottom" ? 'top' : "bottom"));
        if (this.healthBar)
            document.body.removeChild(this.healthBar);

    }

});
function make_ship(x, y, side) {
    return extend(extend(shipObject, {
        sprite: side == "bottom" ? {
            x: 50, y: 221, w: 153, h: 53,
            image: shipImage,
        } : {
            x: 383, y: 12, w: 119, h: 52,
            image: shipImage,
        },
    }), {
        name: side == "bottom" ? 'bottomShip' : 'topShip',
        x: x,
        y: y,
        side: side,
        healthBar: make_healthBar((x + 0.01), (y-0.003), 0.11, 0.003),
    });
}
var ballObject = extend(circleObject, {
    name: "ball",
    x: 0.5,
    y: 0.7,
    r: 0.03,
    vx: 0.0003,
    vy: -0.0003,
    side: "neutral",
    bounce: function(setVel, isVertical = true) {
        soundManager.play('hit');
        this.side = "neutral";
        if (isVertical) {
            this.vy = setVel
            this.vy = 0.0003 * this.vy / Math.abs(this.vy);
        }
        else {
            this.vx = setVel
        }
    },
    update: function(dt, that) {
        this.sprite.update(dt);
        if (this.x - this.r < 0 || this.x + this.r > 1) {
            this.bounce(this.x - this.r < 0 ? Math.abs(this.vx): -Math.abs(this.vx), false);
        }
        ballObject.__proto__.update(dt, that);
    },
    draw: function() {
        this.sprite.draw(this);
    },
    sprite: {
        image: ballImage,
        redImage: redBalls,
        greenImage: greenBalls,
        sheetFrames: [
            {x: 14, y: 208, w: 163, h:162},
            {x: 203, y:205, w: 169, h:168},
            {x: 393, y: 203, w: 174, h:173},
            {x: 583, y: 201, w: 178, h:177},
            {x: 773, y:199, w:181, h:180}
        ],
        timeSinceUpdate: 0,
        frameInd: 0,
        frameIndInc: 1,
        sizeScale: 0.15,
        update: function(dt) {
            this.timeSinceUpdate += dt;
            if (this.timeSinceUpdate > 20) {
                if (this.frameInd + this.frameIndInc < 0 || this.frameInd + this.frameIndInc >= this.sheetFrames.length) {
                    this.frameIndInc = -this.frameIndInc;
                }
                this.frameInd += this.frameIndInc;
                this.timeSinceUpdate = 0;
            }
        },
        draw: function(b) {
            var frame = this.sheetFrames[this.frameInd];
            var image = this.greenImage;
            if (b.side == "bottom") {
                image = this.image;
            }
            if (b.side == 'top') {
                image = this.redImage;
            }
            ctx.drawImage(image, frame.x, frame.y, frame.w, frame.h, (b.x - b.r)*cw, (b.y-b.r)*ch + 0.008*ch, 2*b.r*cw, 2*b.r*cw);
        }
    }
});
var ball = extend(ballObject, {
    name: "ball",
});

function make_explosionObject() {
    return extend(particleObject, {
        name: "explosion",
        sprite: {
            image: image1,
            x: 0, y: 117, w: 39, h: 39,
            dw: 39*1.2, dh: 39*1.2,
            lastFrameX: 13 * 39,
        },
        timeSinceUpdate: 0,
        update: function(dt, that) {
            that.timeSinceUpdate += dt;
            if (that.timeSinceUpdate > 20) {
                that.sprite.x += that.sprite.w;
            }
            if (that.sprite.x > that.sprite.lastFrameX) {
                that.destroyed = true;
            }
            this.__proto__.update(dt, that);
        },
    });
}

function make_explosion(atObj) {
    return Object.assign(make_explosionObject(), {
        x: atObj.x,
        y: atObj.y,
    });
}
