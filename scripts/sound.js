console.log("sound script included!!!");

function make_sound_object(src, volume) {
    var s = {};
    s.sound = document.createElement("audio");
    s.sound.src = src;
    s.sound.setAttribute("preload", "auto");
    s.sound.setAttribute("controls", "none");
    s.sound.style.display = "none";
    document.bovy.appendChild(s.sound);
    s.play = function() {
        s.sound.play();
    };
    s.stop = function() {
        s.sound.pause();
    }
    s.sound.volume = volume;
    return s
}

function clone_sound_object(o) {
    var s = Object.create(o);
    s.sound = o.sound.cloneNode(true);
    return s;
}

var soundManager = {
    audioBuffers: {},
    audiofilesLoaded: 0,
    loadAudio: function (url, name, vol) {
        var request = new XMLHttpRequest();
        request.open('Get', url, true);
        request.responseType = 'arraybuffer';

        request.onload = (function(name, vol) {
            return function() {
                soundManager.audioCtx.decodeAudioData(request.response, (function(name, vol){
                    return function(buffer) {
                        soundManager.audioBuffers[name] = {
                            buffer: buffer,
                            volume: vol,
                        };
                        soundManager.audiofilesLoaded++;
                    };
                })(name, vol)
                , function() {
                    console.log('Error decoding audio data');
                });
            };
        })(name, vol);
        request.send();
    },
    play: function(name) {
        this.play_sound(this.audioBuffers[name]);
    },
    play_sound: function(buffObj) {
        if (this.audiofilesLoaded < 3) return;
        var source = this.audioCtx.createBufferSource();

        var gainNode = this.audioCtx.createGain();
        gainNode.gain.value = buffObj.volume;

        source.buffer = buffObj.buffer;

        source.connect(gainNode);
        gainNode.connect(this.audioCtx.destination);
        source.start(0);
    }
};

window.addEventListener('load', function() {
    soundManager.audioCtx = new AudioContext();
    soundManager.loadAudio('/static/assets/poof.wav', 'poof', 0.8);
    soundManager.loadAudio('/static/assets/pew.mp3', 'pew', 0.1);
    soundManager.loadAudio('/static/assets/hit.wav', 'hit', 0.3);
});
