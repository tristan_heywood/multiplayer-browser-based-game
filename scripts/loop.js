
detect_collisions = function() {

    for (var i = 0; i < objects.length; i++) {
        for (var j = i + 1; j < objects.length; j++) {
            var o1 = objects[i];
            var o2 = objects[j];
            if (!o1.destroyed && !o2.destroyed && o1.side != o2.side &&
                o1.collisionDetector[o2.shape](o1, o2)) {
                if (o2.name in o1.onCollision)
                    o1.onCollision[o2.name](o1, o2);
                if (o1.name in o2.onCollision)
                    o2.onCollision[o1.name](o2, o1);
            }
        }
    }
}

update = function(dt) {
    topPlayerAI.update();

    for(var i = 0; i < objects.length; i++) {
        if (!objects[i].destroyed) {
            objects[i].update(dt, objects[i]);
        }
    }
    detect_collisions();
    newObjects = [];
    for (var i = 0; i < objects.length; i++) {
        if (!objects[i].destroyed) {
            newObjects.push(objects[i]);
        }
    }
    objects = newObjects;

};

draw = function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    //ctx.globalAlpha = 1;
    ctx.drawImage(backgroundImage, 0, 0, canvas.width, canvas.height);
    for (var i = 0; i < objects.length; i++) {
        if (!objects[i].destroyed) {
            objects[i].draw();
        }
    }
    topPlayerAI.draw();
};
function draw_outlines() {
    for (var i = 0; i < objects.length; i++) {
        objects[i].draw_outline();
    }
}

var loopController = {
    lastUpdateTime: 0,
    isPaused: false,
};
loopController.loop = (function(that) {

        return function(curTime) {
            var dt = curTime - that.lastUpdateTime;
            if (curTime == null) {
                requestAnimationFrame(that.loop);
                return;
            }
            that.lastUpdateTime = curTime;
            if (dt <= 0 || dt > 100) {
                requestAnimationFrame(that.loop);
                return;
            }

            if (socketManager.isMaster || true)
                update(dt);

            draw();
            //draw_outlines();


            if (!that.isPaused) {
                requestAnimationFrame(that.loop);
            }
        }
    })(loopController);

document.addEventListener("keydown", (function(controller) {
    return function(e) {
        if (e.key == 'p') {
            if (controller.isPaused) {
                controller.isPaused = false;
                requestAnimationFrame(controller.loop)
            } else {
                console.log('game paused');
                controller.isPaused = true;
            }
        }
    }
})(loopController));
